extends KinematicBody

const WALK_SPEED = 4
const JUMP_DECAY = 0.02
const JUMP_AMOUNT = 900
const GRAVITY = Vector3(0, -9.82, 0)

onready var Score = self.get_node('Score')
onready var Water = self.get_node('/root/World/Water')
onready var Terrain = self.get_node('/root/World/Terrain')

var jumping = Vector3()
var velocity = Vector3()

func _ready():
	"""
	Initialize our player object, and extract position for the currentPosition node
	We also set the current velocity to (0, 0) which will be updated after the goText has been displayed on the screen
	"""
	pass

func update_score(currentPosition):
	"""
	Sends our current position to the score node
	"""
	return Score.update_score(currentPosition)

func update_terrain(currentPosition):
	"""
	Sends our current position to the Terrain node, which then creates or moves the background
	"""
	return Terrain.update_position(currentPosition)

func update_water(currentPosition):
	"""
	Move the water to always be underneath the player
	"""
	Water.transform.origin.x = currentPosition.x

func update_background(currentPosition):
	"""
	Sends our current position to the Background node, which then creates or moves the background
	"""
	return self.get_node('/root/MainGame/Background').update_position(currentPosition)

func apply_left_right(velocity):
	"""
	Apply lateral movement modifiers to the character model
	We'll apply velocity as a fixed value here since we want our character to be responsive to
	left and right movement
	"""
	if Input.is_action_pressed("ui_right"):
		velocity.x = 1.0
	elif Input.is_action_pressed("ui_left"):
		velocity.x = -1.0
	else:
		velocity.x = 0.0

	return velocity

func apply_up_down(velocity):
	"""
	Apply jumping an down acceleration to the character model
	We'll apply velocity here only when the player is on the ground
	When we jump we'll apply velocity to the {self.jumping} which will be slowly removed in self.apply_jumping
	"""
	if not is_on_floor():
		return velocity

	if Input.is_action_pressed("ui_up"):
			self.jumping = Vector3(0, JUMP_AMOUNT, 0)

	return velocity

func get_input(velocity):
	"""
	Increment the players current position and then pass that position to the TerrainSpawner
	We'll pass this the players position each frame, which allows the TerrainSpawner
	to decide whether to spawn more terrain for us or not
	"""
	velocity = apply_up_down(velocity)
	velocity = apply_left_right(velocity)

	velocity *= WALK_SPEED
	velocity.x = clamp(velocity.x, -WALK_SPEED, WALK_SPEED)

	return velocity

func apply_jumping(velocity):
	"""
	Incrementally apply jumping information from the Vector3 created in self.apply_up_down
	"""
	self.jumping *= (1 - JUMP_DECAY)
	return velocity + self.jumping * JUMP_DECAY

func apply_gravity(velocity):
	return velocity + GRAVITY

func _physics_process(delta):
	var newVelocity = Vector3()

	velocity = apply_jumping(apply_gravity(get_input(newVelocity)))

	var currentPosition = self.get_transform().origin
	update_water(currentPosition)
	update_score(currentPosition)

	move_and_slide(velocity, Vector3(0, 1, 0))

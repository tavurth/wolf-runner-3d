extends Node

onready var groundScene = load('res://Ground.tscn')

func get_furthest_right_child():
	"""
	Should return the last child on the x-axis
	"""
	var children = self.get_children()

	## Handle no child case
	if not len(children):
		return false

	var maxItem = children[0]

	for item in self.get_children():
		if item.transform.origin.x > maxItem.transform.origin.x:
			maxItem = item

	return maxItem

func get_furthest_right_position():
	"""
	Returns the furthest right position as a Vector3 of (x, 0, 0)
	"""
	var child = get_furthest_right_child()

	if not child:
		return Vector3(0, 0, 0)

	return Vector3(child.transform.origin.x + child.scale.x, 0, 0)

func terrain_position():
	"""
	Generate a position for the next terrain piece
	Returns a Vector3
	"""
	return Vector3(rand_range(0, 3), rand_range(-0.5, 0.5), 0)

func terrain_difficulty(position):
	return Vector3(position.x + position.x / 50, 0, 0)

func terrain_scaling():
	var random_scale = rand_range(0.5, 1.0)
	return Vector3(random_scale, random_scale, 0)

func get_next_terrain(position = terrain_position()):
	"""
	Returns a new terrain block which the player can stand on
	"""
	##
	## First we'll create an instance of the terrain layer
	## This is an instance of a KinematicBody for collision with the player
	var newTerrain = groundScene.instance()

	##
	## Find the furthest right child so we can create a new terrain
	## That the player can jump to after it
	var basePosition = get_furthest_right_position()

	##
	## Setting the new position for the terrain
	# print(newTerrain.position)
	# newTerrain.set_transform(terrain_position(newTerrain.scale))
	newTerrain.translate(terrain_difficulty(basePosition + position))

	##
	## Make sure that the terrain is deep enough we never see the bottom
	## We'll cover this over with water later
	newTerrain.scale = Vector3(1, 10, 1)

	##
	## Finally, we'll return this new instance
	return newTerrain

func add_initial_terrain():

	##
	## Add the first piece of terrain for the player to start on
	self.add_child(get_next_terrain(Vector3(0, -1, 0)))

	for i in range(0, 200):
		self.add_child(get_next_terrain())

func initialize_terrain():
	self.add_initial_terrain()

func _ready():
	"""
	Initialize our random seed generator and perform initial background setup
	"""
	randomize()
	initialize_terrain()

func update_position(currentPosition):
	pass


func _on_Player_input_event(camera, event, click_position, click_normal, shape_idx):
	pass # replace with function body
